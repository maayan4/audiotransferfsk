#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <float.h>

#include "fourierT.h"

struct complex_number {float re; float im;};

void dft1(float x_in[], struct complex_number *x_k, int k, int N){
	
	for(int i = 0; i < N; i++){
		x_k->re += x_in[i] * cos(2 * M_PI * k * i / N); 
		x_k->im += x_in[i] * sin(2 * M_PI * k * i / N);
	}

	x_k->im = -1 * x_k->im; //since the phrase inside exp is negative. by Euler it equals to cos() - j * sin()
}

void dft(float x_in[], struct complex_number *fourier_vector, int N){
	
	for(int i = 0; i < N; i++){
		dft1(x_in, &fourier_vector[i], i, N);
	}
}

struct complex_number *create_complex_vector(int size){
	struct complex_number *complex_vector = malloc(size * sizeof(struct complex_number)); 
	return complex_vector;
}

int main(){
	return 0;
}

